import React from 'react';
import MainPage from "./pages/MainPage";
import {ThemeProvider} from "@mui/material";
import {appTheme} from "./theme/AppTheme";
import './App.css';

class App extends React.Component {

  render() {
    return (
        <ThemeProvider theme={appTheme}>
          <MainPage/>
        </ThemeProvider>
    );
  }
}

export default App;
