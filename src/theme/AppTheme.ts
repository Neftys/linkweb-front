import {createTheme} from "@mui/material";

/**
* Material custom theme that will be used for the whole site.
* @author Alizée Grand
* @created 22/05/2022
*/
export const appTheme = createTheme({
    palette : {
        primary: {
            main: '#923EFF',
            contrastText: '#FFEFD7'
        },
        secondary: {
            main: '#FF71B1',
            contrastText: '#370644'
        },
        background : {
            paper: '#FF71B1',
            default: '#000000'
        },
        text: {
            primary: '#370644'
        }
    }
})
