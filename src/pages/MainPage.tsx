import React from "react";
import {BrowserRouter, Outlet} from "react-router-dom";
import MainRouter from "../routers/MainRouter";
import MenuDesktop from "../components/menu/MenuDesktop";
import MenuMobile from "../components/menu/MenuMobile";

/**
 *
 * @author Alizée Grand
 * @creation_date 21/05/2022
 **/
class MainPage extends React.Component<{}> {

    state = {
        width: 0,
        height: 0,
    }

    /**
     * Function called during the initialization of the component.
     * Retrieves and stores the size of the window.
     * @param props
     */
    constructor(props: any) {
        super(props);
        this.state = { width: 0, height: 0 };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    /**
     * Function called when the component is displayed for the first time.
     * Starts listening to the "change in size" event of the window.
     */
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    /**
     * Function called when the component is destroyed.
     * Removes listening to the "change in size" event of the window.
     */
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    /**
     * Saves the dimensions of the window in the component.
     */
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }


    render() {

        return (
            <BrowserRouter>
                {
                    // Displays the appropriate menu depending on the size of the window.
                    this.state.width >= 1100 ? <MenuDesktop/> : <MenuMobile/>
                }
                <MainRouter/>
                <Outlet/>
            </BrowserRouter>
        );

    }

}

export default MainPage;