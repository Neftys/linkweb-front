import React, {useEffect, useState} from 'react';
import {useNavigate, useParams} from "react-router-dom";
import {Category} from "../models/Category";
import {ICategoryMini} from "../models/ICategoryMini";
import CategoryService from "../services/api/CategoryService";
import RouteService from "../services/RouteService";
import FormCategory from "../components/categories/FormCategory";

/**
 *
 * @author Alizée Grand
 * @created 22/05/2022
 **/
function EditCategory() {

    const {id} = useParams();
    const navigate = useNavigate();
    const [category, setCategory] = useState<Category>();
    const [miniCategory, setMiniCategory] = useState<ICategoryMini>();

    useEffect(() => {
        if(id !== undefined) {
            CategoryService.getCategoryById(parseInt(id))
                .then(category => {
                    setCategory(category);
                    if(category === undefined) {
                        navigate(RouteService.getCreateCategoryUrl(), {replace: true});
                    } else {
                        const newMiniCategory: ICategoryMini = {
                            title: category.name,
                            color: category.color
                        };
                        setMiniCategory(newMiniCategory);
                    }
                })
                .catch(err => {
                    console.log(err);
                })
            ;
        }

    }, []);

    const onValidate = (categoryMini: ICategoryMini) => {
        if(categoryMini.title && categoryMini.color && Category.isColorCorrect(categoryMini.color) && category) {
            const newCategory = new Category(
                category.id,
                categoryMini.title,
                categoryMini.color
            );
            CategoryService.updateCategory(newCategory)
                .catch(err => {
                    console.log(err);
                })
            ;
        }
    };

    return (
        <div>
            <h1 className="form-title">Modifier catégorie "{category?.name}"</h1>
            <FormCategory initialCategory={miniCategory} onValidate={onValidate}/>
        </div>
    );



}

export default EditCategory;
