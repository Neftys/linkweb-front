import React from "react";
import {Article} from "../models/Article";
import './Create.css';
import ArticleService from "../services/api/ArticleService";
import FormArticle from "../components/articles/FormArticle";
import {IArticleMini} from "../models/IArticleMini";

/**
 *
 * @author Alizée Grand
 * @creation_date 19/05/2022
 **/
class CreateArticle extends React.Component<{}> {

    render() {

        const onValidate = (articleMini: IArticleMini) => {
            const now = new Date();
            if(articleMini.title && articleMini.description && articleMini.categories) {
                const newArticle = new Article(
                    now.getTime(),
                    articleMini.title,
                    articleMini.description,
                    articleMini.categories,
                    now,
                    articleMini.content
                );
                ArticleService.createArticle(newArticle)
                    .catch(err => {
                        console.log(err)
                    })
                ;
            }
        }

        return (
            <div>
                <h1 className="form-title">Créer article</h1>
                <FormArticle onValidate={onValidate}/>
            </div>
        );

    }

}

export default CreateArticle;
