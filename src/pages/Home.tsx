import React from "react";
import ArticleCard from "../components/articles/ArticleCard";
import {Article} from "../models/Article";
import ArticleService from "../services/api/ArticleService";
import './Home.css';
import {Category} from "../models/Category";
import CategoryService from "../services/api/CategoryService";
import HomeCategory from "../components/categories/HomeCategory";

/**
 *
 * @author Alizée Grand
 * @creation_date 19/05/2022
 **/
class Home extends React.Component<{}, {
    allArticles: Article[],
    allCategories: Category[]
}> {

    constructor(props: any) {
        super(props);
        this.state = {allArticles: [], allCategories: []};
    }

    componentDidMount() {
        ArticleService.getArticles()
            .then((articles) => {
                this.setState({allArticles: articles});
            })
            .catch(err => {
                //TODO: message d'erreur dans le front
                console.log(err);
            })
        ;

        CategoryService.getCategories()
            .then((categories) => {
                this.setState({allCategories: categories});
            })
            .catch(err => {
                //TODO: message d'erreur dans le front
                console.log(err);
            })
        ;
    }

    render() {

        const getColor = () => {
            const colors = ["#e60e5d","#e57e17", "#923EFF", "#FF71B1", "#FFD23F", "#3BCEAC", "#0EAD69"];
            const random = Math.floor(Math.random() * 6);
            return colors[random];
        }

        const filterArticles = (categoryId: number) => {
            CategoryService.getAllArticlesByCategoryId(categoryId)
                .then((articles) => {
                    this.setState({allArticles: articles});
                })
                .catch(err => {
                    //TODO: message d'erreur dans le front
                    console.log(err);
                })
            ;
        }

        return (
            <div id="home-main-container">
                <div id="home-category-container">
                    {
                        this.state.allCategories.map((category, index) => (
                            <HomeCategory filterArticles={filterArticles} category={category} key={index}/>
                        ))
                    }
                </div>
                <div id="home-article-container">
                {
                    this.state.allArticles.map((article, index) => (
                        <ArticleCard key={index} article={article} color={getColor()}/>
                    ))
                }
                </div>
            </div>
        );
    }

}

export default Home;
