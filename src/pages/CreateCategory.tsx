import React from 'react';
import {BASIC_CATEGORY_COLOR, Category} from "../models/Category";
import InputCategoryTitle from "../components/categories/InputCategoryTitle";
import InputCategoryColor from "../components/categories/InputCategoryColor";
import {Check} from "@mui/icons-material";
import {Button} from "@mui/material";
import './Create.css';
import CategoryService from "../services/api/CategoryService";
import {ICategoryMini} from "../models/ICategoryMini";
import FormCategory from "../components/categories/FormCategory";

/**
 *
 * @author Alizée Grand
 * @created 21/05/2022
 **/
class CreateCategory extends React.Component<{}> {

    render() {

        const onValidate = (categoryMini: ICategoryMini) => {
            const now = new Date();
            if(categoryMini.title && categoryMini.color) {
                const newCategory = new Category(
                    now.getTime(),
                    categoryMini.title,
                    categoryMini.color
                );
                CategoryService.createCategory(newCategory)
                    .catch(err => {
                        console.log(err)
                    })
                ;
            }
        }

        return (
            <div>
                <h1 className="form-title">Créer catégorie</h1>
                <FormCategory onValidate={onValidate}/>
            </div>
        );

    }

}

export default CreateCategory;
