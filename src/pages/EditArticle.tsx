import React, {useEffect, useState} from 'react';
import {useNavigate, useParams} from "react-router-dom";
import ArticleService from "../services/api/ArticleService";
import {Article} from "../models/Article";
import RouteService from "../services/RouteService";
import FormArticle from "../components/articles/FormArticle";
import {IArticleMini} from "../models/IArticleMini";

/**
 *
 * @author Alizée Grand
 * @created 22/05/2022
 **/
function EditArticle() {

    const {id} = useParams();
    const navigate = useNavigate();
    const [article, setArticle] = useState<Article>();
    const [miniArticle, setMiniArticle] = useState<IArticleMini>();

    useEffect(() => {
        if(id !== undefined){
            ArticleService.getArticleById(parseInt(id))
                .then(article => {
                    setArticle(article);
                    if(article === undefined) {
                        navigate(RouteService.getCreateArticleUrl(), {replace: true});
                    }
                    else {
                        const newMiniArticle: IArticleMini = {
                            title: article.title,
                            description: article.description,
                            categories: article.categories,
                            content: article.content
                        }
                        setMiniArticle(newMiniArticle);
                    }
                })
                .catch(err => {
                    console.log(err);
                })
            ;
        }
    }, []);

    const onValidate = (articleMini: IArticleMini) => {
        if(articleMini.title && articleMini.description && articleMini.categories && article) {
            const newArticle = new Article(
                article.id,
                articleMini.title,
                articleMini.description,
                articleMini.categories,
                article.creationDate,
                articleMini.content
            );
            ArticleService.updateArticle(newArticle)
                .catch(err => {
                    console.log(err)
                })
            ;
        }
    };

    return (
        <div>
            <h1 className="form-title">Modifier article "{article?.title}"</h1>
            <FormArticle initialArticle={miniArticle} onValidate={onValidate}/>
        </div>
    );

}

export default EditArticle;
