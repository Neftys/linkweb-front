import React from 'react';
import {Category} from "../../models/Category";
import {Button, IconButton, Tooltip} from "@mui/material";
import {Delete, Edit} from "@mui/icons-material";
import './HomeCategory.css';
import CategoryService from "../../services/api/CategoryService";
import RouteService from "../../services/RouteService";

/**
 *
 * @author Alizée Grand
 * @created 22/05/2022
 **/
class HomeCategory extends React.Component<{
    category: Category,
    filterArticles: (categoryId: number) => void,
}> {

    render() {

        const onDeleteClick = () => {
            CategoryService.deleteCategory(this.props.category.id)
                .then((status) => {
                    if(status === 200) {
                        window.location.reload();
                    }
                })
                .catch((err) => {
                    console.log(err);
                })
            ;
        }

        const onEditClick = () => {
            window.location.assign(RouteService.getEditCategoryUrlWithoutParams() + this.props.category.id);
        }

        return (
            <div id="home-category-main-container">
                <Tooltip title="Afficher les articles correspondant">
                    <Button
                        variant="contained"
                        sx={{background: this.props.category.color}}
                        onClick={() => {this.props.filterArticles(this.props.category.id)}}
                    >
                    {
                        this.props.category.name
                    }
                    </Button>
                </Tooltip>
                <Tooltip title="Supprimer catégorie">
                    <IconButton color="error" sx={{color: this.props.category.color}} onClick={onDeleteClick}>
                        <Delete/>
                    </IconButton>
                </Tooltip>
                <Tooltip title="Modifier catégorie">
                    <IconButton sx={{color: this.props.category.color}} onClick={onEditClick}>
                        <Edit/>
                    </IconButton>
                </Tooltip>
            </div>
        );

    }

}

export default HomeCategory;
