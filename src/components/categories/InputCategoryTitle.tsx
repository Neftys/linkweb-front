import React, {ChangeEvent} from 'react';
import {TextField} from "@mui/material";

/**
 *
 * @author Alizée Grand
 * @created 21/05/2022
 **/
class InputCategoryTitle extends React.Component<{
    title?: string,
    onTitleChange: (title: string) => void
}> {

    render() {

        const onTitleChange = (event: ChangeEvent<HTMLInputElement>) => {
            const title = event.target.value;
            if(title !== undefined && title !== '') {
                this.props.onTitleChange(title);
            }
        }

        return (
            <TextField type="text" label="Titre" variant="outlined" value={this.props.title} onChange={onTitleChange} required/>
        );

    }

}

export default InputCategoryTitle;
