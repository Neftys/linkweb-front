import React, {ChangeEvent} from 'react';
import {TextField} from "@mui/material";
import {BASIC_CATEGORY_COLOR, Category} from "../../models/Category";

/**
 *
 * @author Alizée Grand
 * @created 21/05/2022
 **/
class InputCategoryColor extends React.Component<{
    color: string,
    onColorChange: (color: string) => void
}> {

    render() {

        const onColorChange = (event: ChangeEvent<HTMLInputElement>) => {
            const color = event.target.value;
            if(color !== undefined && color !== '' && Category.isColorCorrect(color)) {
                this.props.onColorChange(color);
            }
        }

        return (
            <TextField
                type="color"
                label="Couleur"
                variant="outlined"
                value={this.props.color}
                onChange={onColorChange}
            />
        );

    }

}

export default InputCategoryColor;
