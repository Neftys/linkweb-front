import React from 'react';
import {ICategoryMini} from "../../models/ICategoryMini";
import {BASIC_CATEGORY_COLOR, Category} from "../../models/Category";
import InputCategoryTitle from "./InputCategoryTitle";
import InputCategoryColor from "./InputCategoryColor";
import {Button} from "@mui/material";
import {Check} from "@mui/icons-material";

/**
 *
 * @author Alizée Grand
 * @created 22/05/2022
 **/
class FormCategory extends React.Component<{
    initialCategory?: ICategoryMini,
    onValidate: (miniCategory: ICategoryMini) => void
}, {
    category: ICategoryMini
}> {

    constructor(props: any) {
        super(props);
        this.state = {
            category: {
                title: this.props.initialCategory?.title !== undefined ? this.props.initialCategory.title : "",
                color: this.props.initialCategory?.color !== undefined ? this.props.initialCategory.color : BASIC_CATEGORY_COLOR
            }
        };
    }

    componentDidUpdate(prevProps: Readonly<{ initialCategory?: ICategoryMini }>) {
        if(prevProps.initialCategory !== this.props.initialCategory && this.props.initialCategory !== undefined) {
            this.setState({category: this.props.initialCategory});
        }
    }

    render() {

        const onTitleChange = (title: string) => {
            const currentCategory = this.state.category;
            currentCategory.title = title;
            this.setState({category: currentCategory});
        }

        const onColorChange = (color: string) => {
            if(Category.isColorCorrect(color)) {
                const currentCategory = this.state.category;
                currentCategory.color = color;
                this.setState({category: currentCategory});
            }
        }

        const getColorValue = (
            (this.state.category === undefined || this.state.category.color === undefined) ? BASIC_CATEGORY_COLOR : this.state.category.color
        );

        const onValidate = () => {
            this.props.onValidate(this.state.category);
        }

        return (
            <div>
                <form className="create-form">
                    <InputCategoryTitle title={this.state.category.title} onTitleChange={onTitleChange}/>
                    <InputCategoryColor color={getColorValue} onColorChange={onColorChange}/>
                    <Button type="submit" variant="contained" color="success" onClick={onValidate}>
                        Valider
                        <Check/>
                    </Button>
                </form>
            </div>
        );

    }

}

export default FormCategory;
