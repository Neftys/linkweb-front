import React, {ChangeEvent} from 'react';
import {TextField} from "@mui/material";

/**
 *
 * @author Alizée Grand
 * @created 21/05/2022
 **/
class InputArticleDescription extends React.Component<{
    description?: string,
    onDescriptionChange: (description: string) => void
}> {

    render() {

        const onDescriptionChange = (event: ChangeEvent<HTMLInputElement>) => {
            const description = event.target.value;
            if(description !== undefined && description !== '') {
                this.props.onDescriptionChange(description);
            }
        }

        return (
            <TextField
                type="text"
                label="Description"
                variant="outlined"
                value={this.props.description}
                onChange={onDescriptionChange}
                multiline={true}
                rows={4}
                required
            />
        );

    }

}

export default InputArticleDescription;
