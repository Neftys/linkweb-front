import React, {ChangeEvent} from 'react';
import {TextField} from "@mui/material";

/**
 *
 * @author Alizée Grand
 * @created 21/05/2022
 **/
class InputArticleContent extends React.Component<{
    onContentChange: (content: string) => void,
    content?: string,
}> {

    render() {

        const onContentChange = (event: ChangeEvent<HTMLInputElement>) => {
            const content = event.target.value;
            if(content !== undefined && content !== '') {
                this.props.onContentChange(content);
            }
        }

        return (
            <TextField
                type="text"
                label="Contenu"
                variant="outlined"
                value={this.props.content}
                onChange={onContentChange}
                multiline={true}
                rows={20}
            />
        );

    }

}

export default InputArticleContent;
