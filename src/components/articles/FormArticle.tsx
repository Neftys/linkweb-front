import React from 'react';
import InputArticleTitle from "./InputArticleTitle";
import InputArticleDescription from "./InputArticleDescription";
import InputArticleCategories from "./InputArticleCategories";
import InputArticleContent from "./InputArticleContent";
import {Button} from "@mui/material";
import {Check} from "@mui/icons-material";
import {Category} from "../../models/Category";
import {IArticleMini} from "../../models/IArticleMini";

/**
 *
 * @author Alizée Grand
 * @created 22/05/2022
 **/
class FormArticle extends React.Component<{
    initialArticle?: IArticleMini,
    onValidate: (miniArticle: IArticleMini) => void
},{
    article: IArticleMini
}> {

    constructor(props: any) {
        super(props);
        this.state = {
            article: {
                title: this.props.initialArticle?.title !== undefined ? this.props.initialArticle?.title : "",
                description: this.props.initialArticle?.description !== undefined ? this.props.initialArticle?.description : "",
                categories: this.props.initialArticle?.categories !== undefined ? this.props.initialArticle?.categories : [],
                content: this.props.initialArticle?.content !== undefined ? this.props.initialArticle?.content : ""
            }
        };
    }

    componentDidUpdate(prevProps: Readonly<{ initialArticle?: IArticleMini}>) {
        if(prevProps.initialArticle !== this.props.initialArticle && this.props.initialArticle !== undefined) {
            this.setState({article: this.props.initialArticle});
        }
    }

    render() {

        const onTitleChange = (title: string) => {
            const currentArticle = this.state.article;
            currentArticle.title = title;
            this.setState({article: currentArticle});
        }

        const onDescriptionChange = (description: string) => {
            const currentArticle = this.state.article;
            currentArticle.description = description;
            this.setState({article: currentArticle});
        }

        const onCategoriesChange = (categories: Category[]) => {
            const currentArticle = this.state.article;
            currentArticle.categories = categories;
            this.setState({article: currentArticle});
        }

        const onContentChange = (content: string) => {
            const currentArticle = this.state.article;
            currentArticle.content = content;
            this.setState({article: currentArticle});
        }

        const onValidate = () => {
            this.props.onValidate(this.state.article);
        }

        return (
            <div>
                <form className="create-form">
                    <InputArticleTitle title={this.state.article.title} onTitleChange={onTitleChange}/>
                    <InputArticleDescription description={this.state.article.description} onDescriptionChange={onDescriptionChange}/>
                    <InputArticleCategories categories={this.state.article.categories} onCategoriesChange={onCategoriesChange}/>
                    <InputArticleContent content={this.state.article.content} onContentChange={onContentChange}/>
                    <Button type="submit" variant="contained" color="success" onClick={onValidate}>
                        Valider
                        <Check/>
                    </Button>
                </form>
            </div>
        );

    }

}

export default FormArticle;
