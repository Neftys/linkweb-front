import React, {SyntheticEvent} from 'react';
import {Autocomplete, AutocompleteChangeDetails, AutocompleteChangeReason, TextField} from "@mui/material";
import {Category} from "../../models/Category";
import CategoryService from "../../services/api/CategoryService";
import {CATEGORIES_NUMBER} from "../../models/Article";

/**
 *
 * @author Alizée Grand
 * @created 21/05/2022
 **/
class InputArticleCategories extends React.Component<{
    categories?: Category[],
    onCategoriesChange: (categories: Category[]) => void
}, {
    allCategories: Category[]
}> {

    constructor(props: any) {
        super(props);
        this.state = {allCategories: []};
    }

     componentDidMount() {
        CategoryService.getCategories()
            .then((categories) => {
                this.setState({allCategories: categories});
            })
            .catch((err) => {
                //TODO: Message d'erreur adapté dans le front
                console.log(err);
            })
         ;
     }

    render() {

        const onCategoriesChange = (
            event: SyntheticEvent<Element, Event>,
            value: Category[]
        ) => {
            if(value !== undefined && value.length >= CATEGORIES_NUMBER.MIN && value.length <= CATEGORIES_NUMBER.MAX) {
                this.props.onCategoriesChange(value);
            }
        }

        return (
            <Autocomplete
                multiple
                options={this.state.allCategories}
                getOptionLabel={(category) => category.name}
                value={this.props.categories}
                onChange={onCategoriesChange}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label="Catégories"
                        inputProps={{
                            ...params.inputProps,
                            required: (this.props.categories === undefined || this.props.categories.length === 0)
                        }}
                        required
                    />
                )}
            />
        );

    }

}

export default InputArticleCategories;
