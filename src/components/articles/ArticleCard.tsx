import React from "react";
import {Article} from "../../models/Article";
import {Card, CardActions, CardHeader, CardMedia, Collapse, IconButton, Tooltip} from "@mui/material";
import {Delete, Edit, ExpandLess, ExpandMore} from "@mui/icons-material";
import './ArticleCard.css';
import ArticleService from "../../services/api/ArticleService";
import RouteService from "../../services/RouteService";

/**
 * Component displaying a card with the information of an article.
 * @author Alizée Grand
 * @creation_date 19/05/2022
 **/
class ArticleCard extends React.Component<{
    /**
     * Article to display.
     */
    article: Article,
    /**
     * Card background color (hex format).
     */
    color: string
}> {

    state = {
        isExpanded: false,
    }

    render() {

        /**
         * Sets to true or false the opening of the accordion in the local state of the component.
         */
        const changeExpand = () => {
            this.setState({isExpanded: !this.state.isExpanded});
        };

        /**
         * Icon button to open the accordion.
         */
        const moreIcon = (
            <Tooltip title="Voir plus">
                <IconButton onClick={changeExpand} sx={{marginLeft: 'auto'}}>
                    <ExpandMore/>
                </IconButton>
            </Tooltip>
        );

        /**
         * Icon button to close the accordion.
         */
        const lessIcon = (
            <Tooltip title="Voir moins">
                <IconButton onClick={changeExpand} sx={{marginLeft: 'auto'}}>
                    <ExpandLess/>
                </IconButton>
            </Tooltip>
        );

        /**
         * Selects the right icon to display according to the accordion's opening state.
         */
        const getRightExpandIcon = this.state.isExpanded ? lessIcon : moreIcon;

        /**
         * Function called when the delete button is clicked.
         * Calls API for deleting the selected article.
         */
        const onDeleteClick = () => {
            ArticleService.deleteArticle(this.props.article.id)
                .then((status) => {
                    if(status === 200){
                        window.location.reload();
                    }
                })
                .catch((err) => {
                    console.log(err);
                })
            ;
        }

        /**
         * Function called when the edit button is clicked.
         * Redirects on edit article page.
         */
        const onEditClick = () => {
            window.location.assign(RouteService.getEditArticleUrlWithoutParams() + this.props.article.id);
        }

        /**
         * Returns a random image path.
         */
        const getHeaderImg = () => {
            const covers = [
                "../assets/articleCover/cover1.PNG",
                "../assets/articleCover/cover2.jpg",
                "../assets/articleCover/cover3.jpg",
                "../assets/articleCover/cover4.png",
                "../assets/articleCover/cover5.jpg",
                "../assets/articleCover/cover6.jpg",
            ];
            const random = Math.floor(Math.random() * 6);
            return covers[random];
        }

        /**
         * Returns true if the item has content (not empty) and false otherwise.
         */
        const isThereContent = () => {
            return (
                this.props.article.content !== undefined &&
                this.props.article.content !== null &&
                this.props.article.content.length > 0
            );
        }

        return (
            <div id="article-card-main-container">
                <Card sx={{background: this.props.color}} id="article-card-card">
                    <CardHeader
                        title={this.props.article.title}
                        subheader={this.props.article.description}
                        action={
                            <div>
                                {
                                    this.props.article.categories.map((cat, index) => (
                                        <Tooltip title={cat.name}>
                                            <div style={{width:'20px', height: '20px', background: cat.color}} key={index}/>
                                        </Tooltip>
                                    ))
                                }
                            </div>
                        }
                    />
                    <CardMedia
                        component="img"
                        height="200"
                        image={getHeaderImg()}
                        alt='Article illustration'
                    />
                    <CardActions disableSpacing>
                        <Tooltip title="Éditer">
                            <IconButton onClick={onEditClick}>
                                <Edit/>
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Supprimer">
                            <IconButton onClick={onDeleteClick}>
                                <Delete/>
                            </IconButton>
                        </Tooltip>
                        {
                            isThereContent() ? getRightExpandIcon : ''
                        }
                    </CardActions>
                    {
                        isThereContent() ? (
                            <Collapse id="article-card-collapse" in={this.state.isExpanded} unmountOnExit>
                                {this.props.article.content}
                            </Collapse>
                        ) : ''
                    }
                </Card>
            </div>
        );

    }

}

export default ArticleCard;
