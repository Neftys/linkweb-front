import React from "react";
import {Drawer, IconButton} from "@mui/material";
import {Menu} from "@mui/icons-material";
import Navigation from "./nav/Navigation";
import './MenuMobile.css';

/**
 *
 * @author Alizée Grand
 * @creation_date 21/05/2022
 **/
class MenuMobile extends React.Component<{}> {

    state = {
        isDrawerOpen: false,
    }

    render() {

        /**
         * Changes the visibility of the drawer and stores the new one.
         * @param visibility {boolean | undefined} The visibility of the drawer. If it is not filled in, changes the current one (becomes invisible if visible and vice versa).
         */
        const setDrawerVisibility = (visibility?: boolean) => {
            this.setState({isDrawerOpen: visibility !== undefined ? visibility : !this.state.isDrawerOpen});
        }

        /**
         * Properties (style) of the background used by the drawer.
         */
        const paperProps = {
            style: {
                width: '50%',
                backgroundColor: '#FFEFD7'
            }
        };

        return (
            <div id="menu-mobile-main-container">
                <img src="http://localhost:3000/assets/logo/main-logo.png" alt="Logo" id="menu-mobile-logo"/>
                <IconButton color="primary" onClick={() => {setDrawerVisibility()}}><Menu/></IconButton>
                <Drawer
                    id="menu-mobile-drawer"
                    open={this.state.isDrawerOpen}
                    anchor="left"
                    onClose={() => {setDrawerVisibility(false)}}
                    PaperProps={paperProps}
                    onClick={() => {setDrawerVisibility(false)}}
                >
                    <Navigation
                        navId="menu-mobile-nav"
                        linkClassName="menu-mobile-link"
                        linkClassNameActive="menu-mobile-link-active"
                        iconClassName="menu-mobile-icon"
                    />
                </Drawer>
            </div>
        );

    }

}

export default MenuMobile;
