import React from "react";
import MenuNavDesktop from "./nav/MenuNavDesktop";
import './MenuDesktop.css';

/**
 *
 * @author Alizée Grand
 * @creation_date 21/05/2022
 **/
class MenuDesktop extends React.Component<{}> {

    render() {

        return (
            <div id="menu-main-container">
                <div id="menu-desktop-logo-container">
                    <img src="http://localhost:3000/assets/logo/main-logo.png" alt="Logo" id="menu-desktop-logo"/>
                </div>
                <div id="menu-nav-container">
                    <MenuNavDesktop/>
                </div>
            </div>
        );

    }

}

export default MenuDesktop;
