import React from "react";
import {NavLink} from "react-router-dom";
import RouteService from "../../../services/RouteService";
import {Article, Home, LabelImportant} from "@mui/icons-material";

/**
 *
 * @author Alizée Grand
 * @creation_date 21/05/2022
 **/
export default function Navigation(props:
    {
        /**
         * Identifier of the nav tag framing the navigation.
         */
        navId?: string,
        /**
         * Links css class.
         */
        linkClassName?: string,
        /**
         * Css class used when the link is active.
         */
        linkClassNameActive?: string,
        /**
         * Icon css class.
         */
        iconClassName?: string,
    }
) {
    return (
        <nav id={props.navId}>

            <NavLink
                to={RouteService.getHomeUrl()}
                className={({isActive}) => isActive ? props.linkClassNameActive : props.linkClassName}
            >
                <Home className={props.iconClassName}/>
                Accueil
            </NavLink>

            <NavLink
                to={RouteService.getCreateArticleUrl()}
                className={({isActive}) => isActive ? props.linkClassNameActive : props.linkClassName}
            >
                <Article className={props.iconClassName}/>
                Nouvel article
            </NavLink>

            <NavLink
                to={RouteService.getCreateCategoryUrl()}
                className={({isActive}) => isActive ? props.linkClassNameActive : props.linkClassName}
            >
                <LabelImportant className={props.iconClassName}/>
                Nouvelle catégorie
            </NavLink>



        </nav>
    );

}