import React from "react";
import Navigation from "./Navigation";
import './MenuNavDesktop.css';

/**
 *
 * @author Alizée Grand
 * @creation_date 21/05/2022
 **/
class MenuNavDesktop extends React.Component<{}> {

    render() {

        return (
            <div id="menu-nav-desktop-main-container">
                <Navigation
                    navId="menu-nav-desktop-nav"
                    linkClassName="menu-nav-desktop-link"
                    linkClassNameActive="menu-nav-desktop-link-active"
                    iconClassName="menu-nav-desktop-icon"
                />
            </div>
        );

    }

}

export default MenuNavDesktop;