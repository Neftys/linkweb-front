import Config from "./Config";
import {Article} from "../../models/Article";

class ArticleService {

    private url = Config.url_articles;

    public async getArticles(): Promise<Article[]> {
        let articles: Article[] = [];
        await Config.instance.get(this.url)
            .then((res) => {
                if(res.data) {
                    for(let i = 0; i < res.data.length; i++) {
                        const article = Article.remapObjectToArticle(res.data[i]);
                        if(article) {
                            articles.push(article);
                        }
                    }
                }
            })
            .catch(err => {
                console.log(err);
            })
        ;
        return articles;
    }

    public async getArticleById(id: number): Promise<Article | undefined> {
        let article: Article | undefined = undefined;
        await Config.instance.get(this.url + "/" + id)
            .then((res) => {
                if(res.data) {
                    const remapArticle = Article.remapObjectToArticle(res.data);
                    if(remapArticle) {
                        article = remapArticle;
                    }
                }
            })
            .catch(err => {
                console.log(err);
            })
        ;
        return article;
    }

    public async createArticle(article: Article): Promise<number> {
        let status = 500;
        await Config.instance.post(this.url, this.generateFormattedObject(article))
            .then((res) => {
                status = res.status;
            })
            .catch(err => {
                status = err.response.status;
            })
        return status;
    }

    public async updateArticle(article: Article): Promise<number> {
        let status = 500;
        await Config.instance.put(this.url + "/" + article.id, this.generateFormattedObject(article))
            .then((res) => {
                status = res.status;
            })
            .catch(err => {
                status = err.response.status;
            })
        return status;
    }

    public async deleteArticle(id: number): Promise<number> {
        let status = 500;
        await Config.instance.delete(this.url + "/" + id)
            .then((res) => {
                status = res.status;
            })
            .catch(err => {
                status = err.response.status;
            })
        ;
        return status;
    }

    public async deleteAllArticles(): Promise<number> {
        let status = 500;
        await Config.instance.delete(this.url + "/")
            .then((res) => {
                status = res.status;
            })
            .catch(err => {
                status = err.response.status;
            })
        return status;
    }

    private generateFormattedObject(article: Article): object {
        const categoriesId = article.categories.map(cat => cat.id);
        console.log(categoriesId);
        return {
            id: article.id,
            title: article.title,
            description: article.description,
            createdAt: article.creationDate,
            content: article.content,
            categories: categoriesId
        };
    }


}

export default new ArticleService();
