import axios from "axios";

class Config {

    public instance = axios.create({
        baseURL: "http://127.0.0.1:8080/api/",
        headers: {
            "Content-Type": "application/json",
        },
    });

    public url_categories = "categories";
    public url_articles = "articles";

}
export default new Config();
