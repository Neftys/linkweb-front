import Config from "./Config";
import {Category} from "../../models/Category";
import {Article} from "../../models/Article";

class CategoryService {

    private url = Config.url_categories;

    public async getCategories(): Promise<Category[]> {
        let categories: Category[] = [];
        await Config.instance.get(this.url)
            .then((res) => {
                if(res.data) {
                    for(let i = 0; i < res.data.length; i++) {
                        const category = Category.remapObjectToCategory(res.data[i]);
                        if(category) {
                            categories.push(category);
                        }
                    }
                }
            })
            .catch(err => {
                console.log(err);
            })
        ;
        return categories;
    }

    public async getCategoryById(id: number): Promise<Category | undefined> {
        let category: Category | undefined = undefined;
        await Config.instance.get(this.url + "/" + id)
            .then((res) => {
                if(res.data) {
                    const remapCategory = Category.remapObjectToCategory(res.data);
                    if(remapCategory) {
                        category = remapCategory;
                    }
                }
            })
            .catch(err => {
                console.log(err);
            })
        ;
        return category;
    }

    public async getAllArticlesByCategoryId(id: number): Promise<Article[]> {
            let articles: Article[] = [];
            await Config.instance.get(this.url + "/" + id)
                .then((res) => {
                    if(res.data && res.data.articles) {
                        const category = {
                            id: res.data.id,
                            color: res.data.color,
                            name: res.data.title,
                            slug: res.data.slug,
                        };
                        for(let i = 0; i < res.data.articles.length; i++) {
                            const article = res.data.articles[i];
                            article.categories = [category];
                            const articleRemap = Article.remapObjectToArticle(article);
                            if(articleRemap) {
                                articles.push(articleRemap);
                            }
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                })
            ;
            return articles;
        }

    public async createCategory(category: Category): Promise<number> {
        let status = 500;
        await Config.instance.post(this.url, this.generateFormattedObject(category))
            .then((res) => {
                status = res.status;
            })
            .catch(err => {
                status = err.response.status;
            })
        return status;
    }

    public async updateCategory(category: Category): Promise<number> {
        let status = 500;
        await Config.instance.put(this.url + "/" + category.id, this.generateFormattedObject(category))
            .then((res) => {
                status = res.status;
            })
            .catch(err => {
                status = err.response.status;
            })
        return status;
    }

    public async deleteCategory(id: number): Promise<number> {
        let status = 500;
        await Config.instance.delete(this.url + "/" + id)
            .then((res) => {
                status = res.status;
            })
            .catch(err => {
                status = err.response.status;
            })
        return status;
    }

    public async deleteAllCategories(): Promise<number> {
        let status = 500;
        await Config.instance.delete(this.url + "/")
            .then((res) => {
                status = res.status;
            })
            .catch(err => {
                status = err.response.status;
            })
        return status;
    }

    private generateFormattedObject(category: Category): object {
        return {
            id: category.id,
            title: category.name,
            slug: category.slug,
            color: category.color
        };
    }

}

export default new CategoryService();
