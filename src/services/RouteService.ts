/**
 *
 * @author Alizée Grand
 * @creation_date 21/05/2022
 **/
class RouteService {

    private _basicUrl = '/blog/';

    get basicUrl(): string {
        return this._basicUrl;
    }

    public getHomeUrl(): string {
        return this.basicUrl + 'accueil';
    }

    public getCreateArticleUrl(): string {
        return this.basicUrl + 'nouvel-article';
    }

    public getCreateCategoryUrl(): string {
        return this.basicUrl + 'nouvelle-categorie';
    }

    public getEditArticleUrlWithParams(): string {
        return this.getEditArticleUrlWithoutParams() + ':id';
    }

    public getEditArticleUrlWithoutParams(): string {
        return this.basicUrl + 'modifier-article/';
    }

    public getEditCategoryUrlWithParams(): string {
        return this.getEditCategoryUrlWithoutParams() + ':id';
    }

    public getEditCategoryUrlWithoutParams(): string {
        return this.basicUrl + 'modifier-categorie/';
    }


}

export default new RouteService();
