import React from "react";
import {Navigate, Route, Routes} from "react-router-dom";
import Home from "../pages/Home";
import RouteService from "../services/RouteService";
import CreateArticle from "../pages/CreateArticle";
import CreateCategory from "../pages/CreateCategory";
import EditArticle from "../pages/EditArticle";
import EditCategory from "../pages/EditCategory";

/**
 *
 * @author Alizée Grand
 * @creation_date 21/05/2022
 **/
class MainRouter extends React.Component<{}> {

    render() {

        return (
            <Routes>
                <Route path='' element={<Navigate to={RouteService.basicUrl}/>}/>
                <Route path={RouteService.basicUrl}>
                    <Route path={RouteService.basicUrl} element={<Home/>}/>
                    <Route path={RouteService.getHomeUrl()} element={<Home/>}/>
                    <Route path={RouteService.getCreateArticleUrl()} element={<CreateArticle/>}/>
                    <Route path={RouteService.getCreateCategoryUrl()} element={<CreateCategory/>}/>
                    <Route path={RouteService.getEditArticleUrlWithParams()} element={<EditArticle/>}/>
                    <Route path={RouteService.getEditCategoryUrlWithParams()} element={<EditCategory/>}/>
                </Route>
            </Routes>
        );

    }

}

export default MainRouter;
