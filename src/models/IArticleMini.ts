import {Category} from "./Category";

export interface IArticleMini {
    title?: string,
    description?: string,
    categories?: Category[],
    content?: string
}
