export interface ICategoryMini {
    title?: string;
    color?: string;
}
