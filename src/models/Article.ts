import {Category} from "./Category";

export enum CATEGORIES_NUMBER {
    MIN = 1,
    MAX = 2
}

export class Article {

    private _id: number;
    private _title: string;
    private _description: string;
    private _categories: Category[];
    private _creationDate: Date;
    private _content: string | undefined;

    //TODO: Rajouter illustration avec système d'upload et de récupération sur l'API

    constructor(id: number, title: string, description: string, categories: Category[], creationDate: Date, content?: string) {
        this._id = id;
        this._title = title;
        this._description = description;
        this._categories = Article.getRightCategories(categories);
        this._creationDate = creationDate;
        this._content = content;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get categories(): Category[] {
        return this._categories;
    }

    set categories(value: Category[]) {
        this._categories = Article.getRightCategories(value);
    }

    get creationDate(): Date {
        return this._creationDate;
    }

    set creationDate(value: Date) {
        this._creationDate = value;
    }

    get content(): string | undefined {
        return this._content;
    }

    set content(value: string | undefined) {
        this._content = value;
    }

    private static getRightCategories(categories: Category[]): Category[] {
        if(categories.length > CATEGORIES_NUMBER.MAX) {
            categories = categories.splice(0, 2);
        }
        return categories;
    }

    public static remapObjectToArticle(object: any): Article | undefined {
        let article: Article | undefined = undefined;
        if(object.id && object.title && object.description && object.createdAt && object.categories){
            const categories: Category[] = [];
            for(let i = 0; i < object.categories.length; i++) {
                const category = Category.remapObjectToCategory(object.categories[i]);
                if(category) {
                    categories.push(category);
                }
            }
            article = new Article(
                object.id,
                object.title,
                object.description,
                categories,
                object.createdAt,
                object.content
            );
        }
        return article;
    }
}
