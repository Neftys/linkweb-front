export const BASIC_CATEGORY_COLOR = "#FA2515";

export class Category {

    private _id: number;
    private _name: string;
    private _slug: string;
    private _color: string;

    constructor(id: number, name: string, color?: string, slug?: string) {
        this._id = id;
        this._name = name;
        this._slug = slug ? slug : this.generateSlug(name);
        this._color = color !== undefined ? color : BASIC_CATEGORY_COLOR;
    }
    
    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get slug(): string {
        return this._slug;
    }

    set slug(value: string) {
        this._slug = value;
    }

    get color(): string {
        return this._color;
    }

    set color(value: string) {
        this._color = value;
    }

    private generateSlug(name: string): string {
        let regex = /[\s"'_]/g;
        let slug = name.replace(regex, '-');
        return slug.toLowerCase();
    }

    public static isColorCorrect(color: string): boolean {
        let isCorrect = false;
        let regex = /^(#)[A-Fa-f0-9]{6}$/;
        const match = regex.exec(color);
        if(match) {
            isCorrect = true;
        }
        return isCorrect;
    }

    public static remapObjectToCategory(object: any): Category | undefined {
        let category: Category | undefined = undefined;
        if(object.id && object.title && object.slug) {
            category = new Category(
                object.id,
                object.title,
                object.color,
                object.slug
            );
        }
        return category;
    }
}
